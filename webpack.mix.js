const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

const vendor = 'vendor/youpks/core/resources/';
const resources = 'resources/';

const publicJs = 'public/js';
const publicCss = 'public/css';



mix.js(`${resources}js/scripts.js`, publicJs)
    .sass(`${resources}sass/styles.scss`, publicCss)
    .js(`${vendor}js/youpks.js`, publicJs)
    .sass(`${vendor}sass/youpks.scss`, publicCss);

