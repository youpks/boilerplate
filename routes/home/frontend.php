<?php

\Route::get('', [
    'as' => 'frontend.home.index',
    'uses' => 'Core\Controllers\Frontend\HomeController@index',
]);
