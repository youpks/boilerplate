(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[1],{

/***/ "./modules/product/resources/js/scripts.js":
/*!*************************************************!*\
  !*** ./modules/product/resources/js/scripts.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

$('#image-input').on('change', function () {
  $('#image-edit-preview').hide();
  $('#image-preview').attr('src', window.URL.createObjectURL(this.files[0]));
});

/***/ })

}]);