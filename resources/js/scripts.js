try {
    window.$ = window.jQuery = require('jquery');
    require('bootstrap');
    const axios = require('axios').default;
} catch (e) {}

// Modules
import modules from "./modules/modules";
modules();

const  $document = $(document);

// Custom Jquery
$document.ready(function() {

});

