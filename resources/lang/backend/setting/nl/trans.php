<?php

return [

    'title' => [
        'index' => 'Instellingen',
        'site' => 'Site',
        'social' => 'Social media',
        'contact' => 'Contact',
    ],

    'button' => [
        'back' => 'Terug',
        'send' => 'Versturen',
    ],

    'label' => [
        'site' => [
            'name' => 'Naam',
            'url' => 'Url',
        ],
        'social' => [
            'facebook' => 'Facebook',
            'youtube' => 'Youtube',
            'instagram' => 'Instagram',
        ],
        'contact' => [
            'address' => 'Adres',
            'zipcode' => 'Postcode',
            'city' => 'Stad',
            'telephone' => 'Telefoon',
            'email' => 'Email',
        ],
    ],

    'message' => [
        'updated' => 'De instellingen zijn geupdate!',
    ],

];
