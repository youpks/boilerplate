<?php

return [

    'title' => [
        'index' => 'Settings',
        'site' => 'Site',
        'social' => 'Social media',
        'contact' => 'Contact',
    ],

    'button' => [
        'back' => 'Back',
        'send' => 'Send',
    ],

    'label' => [
        'site' => [
            'name' => 'Name',
            'url' => 'Url',
        ],
        'social' => [
            'facebook' => 'Facebook',
            'youtube' => 'Youtube',
            'instagram' => 'Instagram',
        ],
        'contact' => [
            'address' => 'Address',
            'zipcode' => 'Zipcode',
            'city' => 'City',
            'telephone' => 'Telephone',
            'email' => 'Email',
        ],
    ],

    'message' => [
        'updated' => 'The settings are updated!',
    ],

];