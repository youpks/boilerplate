<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
    <div class="container">
        @include('includes::header-logo')
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">
                @if(Auth::check())
                    <li class="mx-lg-3 font-weight-bold text-uppercase">
                        <a href="{{ route('backend.home.index') }}">{{ __("core:home:backend::trans.title.index") }}</a>
                    </li>
                @endif
            </ul>
            @include('includes::locale-select')
        </div>
    </div>
</nav>
