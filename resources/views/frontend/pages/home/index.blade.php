@extends('layouts:frontend::master')
@section('meta_title', 'Home')
@section('content')
    <section class="page-home">
        <div class="container">
            <div class="row justify-content-center">
                <h1>{{ __('home:frontend::trans.title.index') }}</h1>
            </div>
        </div>
    </section>
@endsection
