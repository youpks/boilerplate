<a class="navbar-brand" href="{{ route('frontend.home.index') }}">
    {{ !empty(Setting::get('site.name')) ? Setting::get('site.name') : env('APP_NAME') }}
</a>
