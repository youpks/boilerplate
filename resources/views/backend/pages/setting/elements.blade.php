<div class="row">
    <div class="col-md-6">

        <div class="col-md-8 offset-4">
            <h3>{{ __('setting:backend::trans.title.site') }}</h3>
        </div>

        {!! Form::group_open('site.name row') !!}
        {!! Form::label('site.name', __('setting:backend::trans.label.site.name'), ['class' => 'col-md-4 text-md-right']) !!}
        <div class="col-md-8">
            {!! Form::text('site[name]', Setting::get('site.name'), []) !!}
        </div>
        {!! Form::group_close() !!}

        {!! Form::group_open('site.url row') !!}
        {!! Form::label('site.url', __('setting:backend::trans.label.site.url'), ['class' => 'col-md-4 text-md-right']) !!}
        <div class="col-md-8">
            {!! Form::text('site[url]', Setting::get('site.url'), []) !!}
        </div>
        {!! Form::group_close() !!}

    </div>

    <div class="col-md-6">

        <div class="col-md-8 offset-4">
            <h3>{{ __('setting:backend::trans.title.contact') }}</h3>
        </div>

        {!! Form::group_open('contact.address row') !!}
        {!! Form::label('contact.address', __('setting:backend::trans.label.contact.address'), ['class' => 'col-md-4 text-md-right']) !!}
        <div class="col-md-8">
            {!! Form::text('contact[address]', Setting::get('contact.address'), []) !!}
        </div>
        {!! Form::group_close() !!}

        {!! Form::group_open('contact.zipcode row') !!}
        {!! Form::label('contact.zipcode', __('setting:backend::trans.label.contact.zipcode'), ['class' => 'col-md-4 text-md-right']) !!}
        <div class="col-md-8">
            {!! Form::text('contact[zipcode]', Setting::get('contact.zipcode'), []) !!}
        </div>
        {!! Form::group_close() !!}

        {!! Form::group_open('contact.city row') !!}
        {!! Form::label('contact.city', __('setting:backend::trans.label.contact.city'), ['class' => 'col-md-4 text-md-right']) !!}
        <div class="col-md-8">
            {!! Form::text('contact[city]', Setting::get('contact.city'), []) !!}
        </div>
        {!! Form::group_close() !!}

        {!! Form::group_open('contact.telephone row') !!}
        {!! Form::label('contact.telephone', __('setting:backend::trans.label.contact.telephone'), ['class' => 'col-md-4 text-md-right']) !!}
        <div class="col-md-8">
            {!! Form::text('contact[telephone]', Setting::get('contact.telephone'), []) !!}
        </div>
        {!! Form::group_close() !!}

        {!! Form::group_open('contact.email row') !!}
        {!! Form::label('contact.email', __('setting:backend::trans.label.contact.email'), ['class' => 'col-md-4 text-md-right']) !!}
        <div class="col-md-8">
            {!! Form::text('contact[email]', Setting::get('contact.email'), []) !!}
        </div>
        {!! Form::group_close() !!}

    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-6">

        <div class="col-md-8 offset-4">
            <h3>{{ __('setting:backend::trans.title.social') }}</h3>
        </div>

        {!! Form::group_open('social.facebook row') !!}
        {!! Form::label('social.facebook', __('setting:backend::trans.label.social.facebook'), ['class' => 'col-md-4 text-md-right']) !!}
        <div class="col-md-8">
            {!! Form::text('social[facebook]', Setting::get('social.facebook'), []) !!}
        </div>
        {!! Form::group_close() !!}

        {!! Form::group_open('social.youtube row') !!}
        {!! Form::label('social.youtube', __('setting:backend::trans.label.social.youtube'), ['class' => 'col-md-4 text-md-right']) !!}
        <div class="col-md-8">
            {!! Form::text('social[youtube]', Setting::get('social.youtube'), []) !!}
        </div>
        {!! Form::group_close() !!}

        {!! Form::group_open('social.instagram row') !!}
        {!! Form::label('social.instagram', __('setting:backend::trans.label.social.instagram'), ['class' => 'col-md-4 text-md-right']) !!}
        <div class="col-md-8">
            {!! Form::text('social[instagram]', Setting::get('social.instagram'), []) !!}
        </div>
        {!! Form::group_close() !!}
    </div>
</div>

<div class="float-right">
    {!! Form::submit(__('setting:backend::trans.button.send'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('backend.setting.index') }}" class="btn btn-dark">{{ __('setting:backend::trans.button.back') }}</a>
</div>
