<div class="card mt-5">
    <div class="card-header">
        <h1>{{ __('sidebar:backend::trans.title.navigation') }}</h1>
    </div>
    <div class="card-body">
        <a href="{{ route('backend.home.index') }}" class="btn btn-primary w-100 d-block mb-3">{{ __("core:home:backend::trans.title.index") }}</a>
        <a href="{{ route('backend.user.index') }}" class="btn btn-primary w-100 d-block mb-3">{{ __("core:user:backend::trans.title.index") }}</a>
        <h3 class="text-center mb-0 mt-4">Modules</h3>
        <hr class="mt-0">
{{--        @include('components::menu-item', ['module' => 'product'])--}}
    </div>
</div>
