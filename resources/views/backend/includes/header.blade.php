<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
    <div class="container">
        @include('includes::header-logo')
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">
            </ul>
           @include('includes::locale-select')

            <!-- Right Side Of Navbar -->
            @if(Auth::check())
                @include('includes:backend::header-dropdown')
                <a href="{{ route('backend.setting.index') }}" title="{{ __('setting:backend::trans.title.index') }}" class="ml-3 mb-0 text-secondary h4"><i class="fas fa-cog"></i></a>
            @endif
        </div>
    </div>
</nav>
