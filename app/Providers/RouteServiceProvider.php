<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    public function boot()
    {

        \Route::group([
            'prefix' => localization()->setLocale(),
            'middleware' => 'web'
        ], function () {
            $this->mapBackendRoutes();
            $this->mapFrontendRoutes();
        });

    }

    private function mapBackendRoutes()
    {

    }

    private function mapFrontendRoutes()
    {
        require_once \RouteHelper::frontend('home');

    }

}
