<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;


class TranslationServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->bootTranslations(
            [
                'home',
                'sidebar',
                'dashboard',
                'setting',
            ]
        );
    }

    /**
     * Boot translations
     * @param array $dirs
     */
    private function bootTranslations($dirs = [])
    {
        foreach($dirs as $dir) {
            $this->loadTranslationsFrom(base_path("resources/lang/backend/$dir/"), "$dir:backend");
            $this->loadTranslationsFrom(base_path("resources/lang/frontend/$dir/"), "$dir:frontend");
        }
    }
}
